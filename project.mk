# -*- makefile -*-
NAME = "Return to app after eventsview"
SUMMARY = "Return to app from the events view"
DESCRIPTION := "This patch re-focuses the previously opened application when returning to the events view. 'Quick Events access' must be enabled."
LICENSE=GPLv2+
VER=3.0.1.14
MAINTAINER=Thaodan
# see https://github.com/sailfishos-patches/patchmanager#categories
CATEGORY=homescreen
